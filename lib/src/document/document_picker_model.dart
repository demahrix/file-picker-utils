import 'dart:collection' show HashMap;

/// `T` doit entre de type `File` ou `String`
class DocumentPickerModel<T> {
  final T file;
  final String nom;
  final String? numero;

  /// Date de creation du document
  final String? date;

  final String? dateExpiration;

  DocumentPickerModel({ required this.file, required this.nom, this.numero, this.date, this.dateExpiration });

  /// On exclude intentionnellemnt le fichier car lors de l'envoi de la request les fichiers ne peuvent pas etre inclus dans le json
  HashMap<String, String> toMap() {
    return HashMap<String, String>()
      ..["nom"] = nom
      ..["numero"] = numero ?? ""
      ..["date"] = date ?? ""
      ..["dateExpiration"] = dateExpiration ?? "";
  }

}