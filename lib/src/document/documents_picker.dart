import 'dart:collection' show HashMap;
import 'dart:io' show File;
import 'package:flutter/material.dart';
import 'package:file_picker_utils/file_picker_utils.dart';
import 'package:file_picker_utils/src/document/document_picker_dialog.dart';

import 'document_picker_model.dart';

/// Permet de selectioner des documents d'un utilisateur, d'une entreprise... CNI, ...
/// `T` est de type `File` ou `String`
@Deprecated("")
class DocumentPickerWidget extends StatefulWidget {

  /// Le nom des documents
  final List<String> documentNames;

  final void Function(HashMap<String, DocumentPickerModel<File>?>) onChanged;

  const DocumentPickerWidget({
    required this.documentNames,
    required this.onChanged
  });

  @override
  _DocumentPickerWidgetState createState() => _DocumentPickerWidgetState();
}

class _DocumentPickerWidgetState extends State<DocumentPickerWidget> {

  late HashMap<String, DocumentPickerModel<File>?> _documents;

  @override
  void initState() {
    super.initState();

    _documents = HashMap();

    for (final name in widget.documentNames)
      _documents[name] = null;
  }

  void _addDocument() async {

    final List<String> fileKeys = [];
    _documents.forEach((key, value) { if (value == null) fileKeys.add(key); });

    final result = await showDocumentPickerDialog(
      context: context,
      documentNames: fileKeys,
      // documentNames: widget.documentNames, // filtrer prendre les elements pas encore enregistrés
      locked: false
    );

    if (result != null) {
      setState(() { _documents[result.nom] = result; });
      widget.onChanged(_documents);
    }
  }


  void _onDelete(String key) {

    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text("Supprimer le document"),
        content: RichText(
          text: TextSpan(
            style: Theme.of(context).textTheme.bodyMedium,
            children: [
              TextSpan(
                text: "Voulez vous supprimer le document "
              ),
              TextSpan(
                text: key,
                style: TextStyle(
                  fontWeight: FontWeight.w600
                )
              ),
              TextSpan(
                text: " ?"
              )
            ]
          ),
        ),
        actions: [
          TextButton(
            onPressed: Navigator.of(context, rootNavigator: true).pop,
            child: Text("Non"),
          ),

          TextButton(
            onPressed: () {
              Navigator.of(context, rootNavigator: true).pop();
              setState(() { _documents[key] = null; });
              widget.onChanged(_documents);
            },
            child: Text("oui"),
          )
        ],
      ),
      
    );



  }

  @override
  Widget build(BuildContext context) {

    /// on remplie cette liste avec les elements non file
    final List<String> fileKeys = [];
    _documents.forEach((key, value) { if (value != null) fileKeys.add(key); });

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          _placeHolder(),

          const SizedBox(height: 20.0),

          Expanded(
            child: fileKeys.isEmpty 
              ? _emptyWidget()
              : SingleChildScrollView(
                  child: _getFilesWidget(fileKeys),
                ),
          )


        ],
      ),
    );
  }

  Widget _placeHolder() {
    return Material(
      color: Color(0xFFF3F3F3),
      borderRadius: BorderRadius.circular(6.0),
      clipBehavior: Clip.hardEdge,
      child: InkWell(
        onTap: _addDocument,
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              const Icon(Icons.file_upload), // attendre le nouveau style
              const SizedBox(width: 8.0),
              const Text(
                "Ajouter des pieces jointes"
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _getFilesWidget(List<String> keys) {
    return Container(
      width: double.infinity,
      child: DataTable(
        headingTextStyle: TextStyle(
          color: Colors.black45,
          fontWeight: FontWeight.w600
        ),
        columns: const [
          DataColumn(label: Text("Nom")),
          DataColumn(label: Text("Numéro")),
          DataColumn(label: Text("Date")),
          DataColumn(label: Text("Date expiration")),
          DataColumn(label: Text("Taille")),
          DataColumn(label: Text("Action"))
        ],
        rows: List.generate(keys.length, (index) {
          final String key = keys[index];
          return DocumentDataRow(data:_documents[key]!, onDelete: _onDelete);
        }, growable: false)
      ),
    );
  }

  static Widget _emptyWidget() { // TODO: bien designer cette section
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [

          Image.asset(
            "assets/empty.png",
            width: 512.0,
            package: "file_picker_utils",
          ),
          const SizedBox(height: 15.0,),
          Text(
            "Aucun document ajouté",
            style: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.w600
            ),
          )

        ],
      ),
    );
  }

}


class DocumentDataRow extends DataRow {
  
  DocumentDataRow({
    required DocumentPickerModel data,
    required ValueSetter<String> onDelete
  }): super(
    cells: [
      DataCell(Row(
        children: [
          SizedBox(child: Image.asset(
            FilePickerUtils.getImageUrl(data.file.path),
            package: "file_picker_utils",
            height: 20.0,
          )),
          const SizedBox(width: 10.0),
          Text(
            data.nom
          )
        ],
      )),
       DataCell(Text(
        data.numero ?? "N/A"
      )),
       DataCell(Text(
        data.date ?? "N/A"
      )),
       DataCell(Text(
        data.dateExpiration ?? "N/A"
      )),
      DataCell(Text(
        FilePickerUtils.getFileSize(data.file)
      )),
      DataCell(TextButton(
        onPressed: () => onDelete(data.nom),
        child: const Text(
          "Supprimer",
          style: TextStyle(
            color: Colors.red
          ),
        )
      ))
    ]
  );

}
