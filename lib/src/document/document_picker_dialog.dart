import 'dart:io' show File;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:file_picker_utils/file_picker_utils.dart';
import 'package:filepicker_windows/filepicker_windows.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:my_input_formatter/my_input_formatter.dart';
import 'document_picker_model.dart';

@Deprecated("")
Future<DocumentPickerModel<File>?> showDocumentPickerDialog<T>({
  required BuildContext context,
  required List<String> documentNames,
  required bool locked
}) {
  return showDialog<DocumentPickerModel<File>?>(
    context: context,
    builder: (_) => DocumentPickerDialog(
      documentNames: documentNames,
      locked: locked,
    )
  );
}

class DocumentPickerDialog extends StatefulWidget {

  final List<String> documentNames;
  final bool locked;

  const DocumentPickerDialog({
    required this.documentNames,
    required this.locked
  });

  @override
  _DocumentPickerDialogState createState() => _DocumentPickerDialogState();
}

class _DocumentPickerDialogState extends State<DocumentPickerDialog> {

  String? _documentName;
  File? _currentFile;
  String _documentNumber = "";
  String _documentDate = "";
  String _expirationDate = "";
  
  static String _dateErrorLabel = "Veuillez entrer une date valide";

  static ButtonStyle _buttonStyle = ButtonStyle(
    padding: MaterialStateProperty.all(EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0)),
    elevation: MaterialStateProperty.all(0.0)
  );

  void _onChooseFile() {
    final result = OpenFilePicker()
      ..filterSpecification = const {
        "Document": "*.pdf;*.doc;*.docx;*.ppt;*.pptx;"
      }
      ..title = "Selectionner un document";

    final file = result.getFile();

    if (file != null) {
      this.setState(() { _currentFile = file; });
    }

  }

  void _onDocumentNameChange(String? newValue) {
    setState(() { _documentName = newValue; });
  }

  bool _isValidDate(String date) {
    if (date == "")
      return true;
    return DateInputFormatter.isValid(date);
  }

  bool _isValid() {
    return _documentName != null
      && _currentFile != null
      && _isValidDate(_documentDate)
      && _isValidDate(_expirationDate);
  }

  void _onDocumentNumberChanged(String newValue) => _documentNumber = newValue;
  void _onDocumentDateChanged(String newValue) => setState(() { _documentDate = newValue; });
  void _onExpirationDateChanged(String newValue) => setState(() { _expirationDate = newValue; });

  void _submit() {
    Navigator.of(context, rootNavigator: true).pop(DocumentPickerModel<File>(
      file: _currentFile!,
      nom: _documentName!,
      numero: _documentNumber.trim(),
      date: _documentDate,
      dateExpiration: _expirationDate
    ));
  }

  // FIXME supprimer la depense `url_launcher`
  @Deprecated("cause de url launcher")
  void _onView() {
    var unixPath = _currentFile!.path.split(r"\").join("/");
    launch("file://$unixPath");
  }

  @override
  Widget build(BuildContext context) {

    final bool fileSelected = _currentFile != null;

    return Dialog(
      child: Container(
        width: 600.0, // FIXME math (65% ou 600) ?
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            const SizedBox(height: 5.0),

            Text(
              "Selectionner un document",
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.w600
              ),
            ),

            const SizedBox(height: 20.0),

            CustomDropdownMenu<String>(
              items: widget.documentNames,
              itemBuilder: (item) => Text(item),
              hintText: "Selectionner le type de documment",
              onChanged: widget.locked ? null : _onDocumentNameChange,
            ),

            const SizedBox(height: 15.0,),

            _filePickerWidget(),

            if (fileSelected)
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: myTextField(
                  "Numéro du document",
                  onChanged: _onDocumentNumberChanged
                ),
              ),

            if (fileSelected)
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: myTextField(
                  "Etablie le",
                  inputFormatters: [ DateInputFormatter() ],
                  prefixIcon: Icon(Icons.calendar_today),
                  onChanged: _onDocumentDateChanged,
                  errorText: _isValidDate(_documentDate) ? null : _dateErrorLabel
                ),
              ),

            if (fileSelected)
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: myTextField(
                  "Date d'expiration",
                  inputFormatters: [ DateInputFormatter() ],
                  prefixIcon: Icon(Icons.calendar_today),
                  onChanged: _onExpirationDateChanged,
                  errorText: _isValidDate(_expirationDate) ? null : _dateErrorLabel
                ),
              ),

            const SizedBox(height: 25.0,),

            _actionWidget()

          ],
        ),
      ),
    );
  }

  
  Widget myTextField(String label, {
    String? errorText,
    Widget? prefixIcon,
    required ValueChanged<String> onChanged,
    List<TextInputFormatter>? inputFormatters
  }) {
    return TextField(
      onChanged: onChanged,
      inputFormatters: inputFormatters,
      decoration: InputDecoration(
        border: const OutlineInputBorder(),
        labelText: label,
        errorText: errorText,
        prefixIcon: prefixIcon
      )
    );
  }

  Widget _filePickerWidget() {
    return InkWell(
      onTap: widget.locked ? null : _onChooseFile,
      child: Container(
        height: 54.0,
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(6.0)),
          border: Border.all(color: Colors.black12)
        ),
        child: _currentFile == null
          ? Center(
              child: Text(
                "Selectionner un ficher",
                style: TextStyle(
                  color: Color(0xffAAAAAA)
                ),
              ),
            )
          : Row(
              children: [

                SizedBox(
                  height: 30.0,
                  child: Image.asset(FilePickerUtils.getImageUrl(_currentFile!.path), package: "file_picker_utils"),
                ),

                const SizedBox(width: 10.0),

                Expanded(
                  child: Text(
                    FilePickerUtils.getFileName(_currentFile!)
                  )
                ),

                SizedBox(
                  child: TextButton(
                    onPressed: _onView,
                    child: Text("voir"),
                  ),
                )

              ],
            )
      ),
    );
  }

  Widget _actionWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [

        OutlinedButton(
          onPressed: () {
            Navigator.of(context, rootNavigator: true).pop(null);
          },
          style: _buttonStyle,
          child: const Text("Annuler")
        ),

        const SizedBox(width: 15.0),

        ElevatedButton(
          onPressed: _isValid() ? _submit : null,
          style: _buttonStyle,
          child: const Text("Enregister")
        )

      ],
    );
  }

}

class CustomDropdownMenu<T> extends DropdownButtonFormField<T> {

  CustomDropdownMenu({
    T? value, // @nullable
    required List<T> items,
    required Widget Function(T) itemBuilder,
    ValueChanged<T?>? onChanged,
    required String hintText
  }): super(
    value: value,
    items: List.generate(items.length, (index) => DropdownMenuItem(value: items[index], child: itemBuilder(items[index])), growable: false),
    onChanged: onChanged,
    decoration: InputDecoration(
      hintText: hintText,
      border: OutlineInputBorder(borderSide: BorderSide.none),
      filled: true
    ),
    // style: TextStyle(
    //   color: Colors.black54,
    // ),
  );
}
