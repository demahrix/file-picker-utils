import 'dart:io' show File;
import 'package:file_picker_utils/file_picker_utils.dart';
import 'package:flutter/material.dart';
import 'package:file_picker_utils/src/file_picker_base.dart';
import 'file_picker_controller.dart';

@Deprecated("")
class SimpleFilePicker extends StatelessWidget {

  final String label;

  final File? initialFile;
  final Map<String, String> filterSpecification;
  final String? defaultExtension;
  final ValueChanged<File?>? onChanged;
  final FilePickerController? controller;

  /// La taille de l'image qui represente le type du fichier
  final double imageSize;

  final int fileNameMaxLines;

  const SimpleFilePicker({
    Key? key,
    required this.label,
    this.initialFile,
    this.filterSpecification = const <String, String>{},
    this.defaultExtension,
    required this.onChanged,
    this.controller,
    this.imageSize: 20.0,
    this.fileNameMaxLines: 1
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return FilePickerBase(
      label: label,
      onChanged: onChanged,
      filterSpecification: filterSpecification,
      placeholder: Row(
        children: [
          Text(label),
          const SizedBox(width: 5.0,),
          const Icon(Icons.arrow_drop_down)
        ],
      ),
      fileSelectedBuilder: _builder,
    );
  }

  Widget _builder(File file, { required VoidCallback deleteFile }) {

    String name = FilePickerUtils.getFileName(file);
    String imageUrl = FilePickerUtils.getImageUrl(file.path);

    return Row(
      children: [

        Image.asset(imageUrl, width: imageSize, package: "file_picker_utils"),

        const SizedBox(width: 5.0),

        Expanded(
          child: Text(
            name,
            overflow: TextOverflow.ellipsis,
            maxLines: fileNameMaxLines,
          ),
        ),

        const SizedBox(width: 5.0),

        GestureDetector(
          onTap: deleteFile,
          child: const Icon(Icons.close, size: 14.0,),
        ),

      ],
    );
  }

}
