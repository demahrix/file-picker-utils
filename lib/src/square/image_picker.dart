import 'dart:io';

import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:z_file/z_file.dart';
import '../file_picker_controller.dart';

// class ImagePicker extends SquareFilePicker {

//   ImagePicker({
//     double width = 120,
//     double height = 120,
//     String label = "Selectionner une image",
//     Map<String, String> filterSpecification = const {
//       "Image": "*.jpg;*.jpeg;*.png;*.webp;"
//     },
//     String? defaultExtension,
//     required ValueChanged<File?> onChanged,
//     FilePickerController? controller,

//     /// Widget visible quand aucun fichier n'est selectionné
//     Widget placeholder = const Icon(Icons.add_photo_alternate),

//     /// Fonction appele quand un fichier est selectionné
//     Widget Function(File)? builder,

//     BoxDecoration? decoration,

//     BorderRadiusGeometry? borderRadius
//   }): super(
//         width: width,
//         height: height,
//         label: label,
//         onChanged: onChanged,
//         controller: controller,
//         placeholder: placeholder,
//         filterSpecification: filterSpecification,
//         defaultExtension: defaultExtension,
//         builder: builder ?? (file) => Image.file(file, fit: BoxFit.cover),
//         decoration: BoxDecoration(color: Colors.black12, borderRadius: borderRadius),
//       );
// }

// SquareFilePicker

/// Use clip oval
class ImagePicker extends StatefulWidget {

  final Zfile? initialImage;
  final double width;
  final double height;
  final String pickerLabel;
  final String label;
  final Map<String, String> filterSpecification;
  final String? defaultExtension;
  final ValueChanged<Zfile?>? onChanged;
  /// FIXME remove if not used
  final FilePickerController? controller;

  const ImagePicker({
    Key? key,
    this.initialImage,
    this.width = 120.0,
    this.height = 120.0,
    this.pickerLabel = 'Sélectionner une image',
    this.label = 'Ajouter une image',
    this.filterSpecification = const {
      "Image": "*.jpg;*.jpeg;*.png;*.webp"
    },
    this.defaultExtension,
    this.onChanged,
    this.controller
  }): super(key: key);

  @override
  State<ImagePicker> createState() => _ImagePickerState();
}

class _ImagePickerState extends State<ImagePicker> {

  Zfile? _image;
  late bool readOnly;

  @override
  void initState() {
    super.initState();
    if (widget.initialImage != null)
      _image = widget.initialImage;
    readOnly = widget.onChanged == null;
  }

  void _handleFilePicker() async {

    // final document = OpenFilePicker()
    //   ..filterSpecification = widget.filterSpecification
    //   ..defaultExtension = widget.defaultExtension
    //   ..title = widget.pickerLabel;

    // final result = document.getFile();

    var result = await openFile(
      acceptedTypeGroups: const [XTypeGroup(
        label: "Images",
        extensions: ['jpg', 'jpeg', 'png', 'webp']
      )]
    );

    if (result != null) {
      this.setState(() { _image = Zfile.fromFile(File(result.path)); });
      widget.onChanged?.call(_image);
    }
  }

  void _deleteCurrentFile() {
    this.setState(() { _image = null; });
    widget.onChanged?.call(null);
  }

  @override
  Widget build(BuildContext context) {
    bool isEmpty = _image == null;

    return Container(
      width: widget.width,
      height: widget.height,
      child: Stack(
        clipBehavior: Clip.none,
        children: [

          GestureDetector(
            onTap: readOnly ? null : _handleFilePicker,
            child: MouseRegion(
              cursor: readOnly ? MouseCursor.defer : SystemMouseCursors.click,
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  border: isEmpty ? Border.all(color: Colors.black12) : null,
                  borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                ),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(6.0)),
                  child: isEmpty
                  ? _getPlaceholder()
                  : _image!.isLocalFile
                    ? Image.file(_image!.file!, fit: BoxFit.cover)
                    : Image.network(_image!.url!, fit: BoxFit.cover),
                )
              ),
            ),
          ),

          if (!isEmpty)
            Positioned(
              top: -10,
              right: -10,
              child: GestureDetector(
                onTap: _deleteCurrentFile,
                child: Container(
                  width: 30.0,
                  height: 30.0,
                  decoration: const BoxDecoration(
                    color: Color(0xff333333),
                    shape: BoxShape.circle
                  ),
                  child: const Icon(
                    Icons.close_outlined,
                    color: Colors.white,
                  ),
                ),
              ),
            )

        ],
      ),
    );
  }

  Widget _getPlaceholder() {
    return Container(
      width: 120.0,
      height: 120.0,
      decoration: BoxDecoration(
        
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [

          Container(
            width: 45.0,
            height: 45.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(4.0))
            ),
            child: Icon(Icons.file_upload_outlined)
          ),

          const SizedBox(height: 8.0),

          Text(
            widget.label,
            style: TextStyle(
              fontSize: 10.0,
              fontWeight: FontWeight.w600
            ),
          )

        ],
      ),
    );
  }

}
