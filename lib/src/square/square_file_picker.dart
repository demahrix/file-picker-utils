import 'dart:io' show File;
import 'package:filepicker_windows/filepicker_windows.dart';
import 'package:flutter/material.dart';
import '../file_picker_controller.dart';

// FIXME: extendre 'FilePickerBase'
class SquareFilePicker extends StatefulWidget {

  final double width;
  final double height;
  final String label;
  final Map<String, String> filterSpecification;
  final String? defaultExtension;
  final ValueChanged<File?> onChanged;
  final FilePickerController? controller;

  /// Widget visible quand aucun fichier n'est selectionné
  final Widget placeholder;

  /// Fonction appele quand un fichier est selectionné
  final Widget Function(File) builder;

  final BoxDecoration? decoration;

  SquareFilePicker({
    this.width = 100.0,
    this.height = 100.0,
    required this.label,
    this.filterSpecification = const {},
    this.defaultExtension,
    required this.onChanged,
    this.controller,
    required this.placeholder,
    required this.builder,
    this.decoration
  });

  @override
  _SquareFilePickerState createState() => _SquareFilePickerState();
}

class _SquareFilePickerState extends State<SquareFilePicker> {

  File? _file;

  void _handleFilePicker() {

    final document = OpenFilePicker()
      ..filterSpecification = widget.filterSpecification
      ..defaultExtension = widget.defaultExtension
      ..title = widget.label;

    final result = document.getFile();
    if (result != null) {
      this.setState(() { _file = result; });
      widget.onChanged(result);
    }
  }

  void _deleteCurrentFile() {
    this.setState(() { _file = null; });
    widget.onChanged(null);
  }

  @override
  Widget build(BuildContext context) {

    bool isEmpty = _file == null;

    return Container(
      width: widget.width,
      height: widget.height,
      child: Stack(
        clipBehavior: Clip.none,
        children: [

          GestureDetector(
            onTap: _handleFilePicker,
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: widget.decoration,
              child: isEmpty
                ? widget.placeholder
                : widget.builder(_file!),
            ),
          ),

          if (!isEmpty)
            Positioned(
              top: -10,
              right: -10,
              child: GestureDetector(
                onTap: _deleteCurrentFile,
                child: Container(
                  width: 30.0,
                  height: 30.0,
                  decoration: const BoxDecoration(
                    color: Color(0xff333333),
                    shape: BoxShape.circle
                  ),
                  child: const Icon(
                    Icons.close_outlined,
                    color: Colors.white,
                  ),
                ),
              ),
            )

        ],
      ),
    );
  }
}
