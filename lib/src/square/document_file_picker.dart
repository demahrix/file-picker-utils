import 'dart:io';
import 'package:flutter/material.dart';
import 'package:file_picker_utils/src/square/square_file_picker.dart';
import 'package:file_picker_utils/src/utils.dart';
import '../file_picker_controller.dart';

class DocumentPicker extends SquareFilePicker {

  DocumentPicker({
    double width = 100,
    double height = 100,
    String label = "Selectionner un document",
    Map<String, String> filterSpecification = const {
      "Document": "*.pdf;*.doc;*.docx;*.ppt;*.pptx;"
    },
    String defaultExtension = "pdf",
    required ValueChanged<File?> onChanged,
    FilePickerController? controller,

    /// Widget visible quand aucun fichier n'est selectionné
    Widget placeholder = const Icon(Icons.article, color: Colors.blue),

    /// Fonction appele quand un fichier est selectionné
    Widget Function(File)? builder
  }): super(
    width: width,
    height: height,
    label: label,
    onChanged: onChanged,
    controller: controller,
    placeholder: placeholder,
    filterSpecification: filterSpecification,
    defaultExtension: defaultExtension,
    builder: builder ?? (file) => _builder(file),
    decoration: BoxDecoration(
      border: Border.all(color: Colors.blue, width: 1.0),
      borderRadius: const BorderRadius.all(Radius.circular(6.0)),
      color: Colors.blue.withOpacity(0.12)
    )
  );

  static Widget _builder(File file) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Center(
            child: Image(
              image: AssetImage("assets/pdf.png", package: "file_picker_utils"),
              width: 36.0,
              height: 36.0,
            ),
          ),

          Column(
            mainAxisSize: MainAxisSize.min,

            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                FilePickerUtils.getFileName(file),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontSize: 12.0
                ),
              ),

              Text(
                FilePickerUtils.getFileSize(file),
                style: const TextStyle(
                  fontSize: 10.0
                ),
              ),

            ],
          )
        ],
      ),
    );
  }

}
