import 'package:z_file/z_file.dart';

class FilePickerController {

  Zfile? file;
  FilePickerController([Zfile? initialFile]);

  late void Function() open;
  late void Function() reset;
  late void Function(Zfile?) setFile;
  late Zfile? Function() getFile;

}
