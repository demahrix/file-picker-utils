import 'dart:io' show File;

import 'package:file_picker_utils/file_picker_utils.dart';
import 'package:flutter/material.dart';

class SelectedFileWidget extends StatelessWidget {

  final File file;
  final ValueSetter<File>? onDelete;

  const SelectedFileWidget({
    Key? key,
    required this.file,
    this.onDelete
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 64.0,
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black26),
        borderRadius: const BorderRadius.all(Radius.circular(4.0))
      ),
      child: Row(
        children: [

          _fileImage(),

          const SizedBox(width: 8.0),

          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  FilePickerUtils.getFileName(file),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 14.0
                  ),
                ),
                Text(
                  FilePickerUtils.getFileSize(file),
                  style: TextStyle(
                    fontSize: 12.0
                  ),
                )
              ],
            ),
          ),

          if (onDelete != null)
            _deleteWidget()

        ],
      ),
    );
  }

  Widget _fileImage() {
    return Container(
      width: 40.0,
      height: 40.0,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.all(Radius.circular(4.0))
      ),
      // FIXME adapte image to each file type
      child: Image.asset('assets/other.png', width: 24.0, height: 24.0, package: 'file_picker_utils', fit: BoxFit.contain, filterQuality: FilterQuality.high),
    );
  }

  Widget _deleteWidget() {
    return IconButton(
      onPressed: () => onDelete!(file),
      icon: Icon(Icons.delete_outline_outlined),
    );
  }

}
