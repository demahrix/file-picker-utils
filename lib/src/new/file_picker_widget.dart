import 'dart:io' show File;
import 'package:file_picker_utils/src/new/selected_file_widget.dart';
import 'package:filepicker_windows/filepicker_windows.dart';
import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';

class FilePickerWidget extends StatefulWidget {

  // add max file size
  // add selected file builder

  final double height;
  final Widget placeholder;
  final String? dialogLabel;
  final int maxFileCount;
  /// ex: `'doc'`
  final String? defaultExtension;
  final Map<String, String> filterSpecification;
  final List<File>? initialFiles;
  final ValueChanged<List<File>>? onChanged;

  const FilePickerWidget({
    Key? key,
    this.height = 186.0,
    this.defaultExtension,
    this.filterSpecification = const {},
    this.placeholder = const Text(
      'Parcourez ou glisser-déposer un fichier ici',
      style: TextStyle(
        color: Colors.black54
      )
    ),
    this.dialogLabel,
    required this.maxFileCount,
    this.initialFiles,
    this.onChanged
  }): super(key: key);

  @override
  State<FilePickerWidget> createState() => _FilePickerWidgetState();
}

class _FilePickerWidgetState extends State<FilePickerWidget> {

  late List<File> _selected;

  @override
  void initState() {
    super.initState();
    _selected = widget.initialFiles ?? [];
  }

  void _onOpenDialog() {

    final document = OpenFilePicker()
      ..filterSpecification = widget.filterSpecification
      ..defaultExtension = widget.defaultExtension
      ..title = widget.dialogLabel ?? "Sélectionner un fichier";

    final result = document.getFile();
    if (result != null) {
      this.setState(() { _selected.add(result); });
      widget.onChanged?.call(_selected);
    }

  }

  void _onDeleteFile(File file) {
    setState(() { _selected.remove(file); });
    widget.onChanged?.call(_selected);
  }

  @override
  Widget build(BuildContext context) {
    var fileCount = _selected.length;
    return Column(
      children: [

        if (fileCount < widget.maxFileCount)
          InkWell(
            onTap: widget.onChanged == null ? null : _onOpenDialog,
            child: DottedBorder(
              dashPattern: [4, 2],
              color: Colors.black54,
              borderType: BorderType.RRect,
              radius: Radius.circular(8.0),
              child: SizedBox(
                width: double.infinity,
                height: widget.height,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [

                      Icon(Icons.cloud_upload_outlined, size: 48.0),
                      const SizedBox(height: 8.0),
                      widget.placeholder

                    ],
                  ),
                ),
              ),
            ),
          ),

        for (int i=0; i<fileCount; ++i)
          Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: SelectedFileWidget(
              onDelete: _onDeleteFile,
              file: _selected[i]
            ),
          )

      ],
    );
  }
}
