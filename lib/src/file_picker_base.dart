import 'package:flutter/material.dart';
import 'dart:io' show File;
import 'package:filepicker_windows/filepicker_windows.dart';
import 'file_picker_controller.dart';

@Deprecated("")
class FilePickerBase extends StatefulWidget {

  /// S'affiche sur la fenetre ouverte sur windows
  final String label;

  final File? initialFile;
  final Map<String, String> filterSpecification;
  final String? defaultExtension;
  final ValueChanged<File?>? onChanged;
  final FilePickerController? controller;

  final bool _disabled;
  final Widget placeholder;

  /// Widget qu'on affiche qu'un fichier est selectionné
  final Widget Function(File, { required VoidCallback deleteFile })? fileSelectedBuilder;

  const FilePickerBase({
    Key? key,
    this.initialFile,
    required this.placeholder,
    required this.label,
    required this.fileSelectedBuilder,
    this.filterSpecification = const <String, String>{},
    this.defaultExtension,
    required this.onChanged,
    this.controller
  }): _disabled = onChanged == null,
      super(key: key);
  /// FIXME Mettre un assert fileSelectedBuilder doit etre different de `null` si 'disabled' est egale à 'true'

  @override
  _FilePickerBaseState createState() => _FilePickerBaseState();
}

class _FilePickerBaseState extends State<FilePickerBase> {

  File? _file;

  @override
  void initState() {
    super.initState();

    if (widget.initialFile != null)
      _file = widget.initialFile;
  }

  void _handleFilePicker() {

    final document = OpenFilePicker()
      ..filterSpecification = widget.filterSpecification
      ..defaultExtension = widget.defaultExtension
      ..title = widget.label;

    final result = document.getFile();
    if (result != null) {
      this.setState(() { _file = result; });
      widget.onChanged?.call(result);
    }
  }

  void _deleteCurrentFile() {
    this.setState(() { _file = null; });
    widget.onChanged?.call(null);
  }

  @override
  Widget build(BuildContext context) {
    bool isEmpty = _file == null;
    return Container(
      child: MouseRegion(
        cursor: MouseCursor.uncontrolled,
        child: GestureDetector( // over
          onTap: widget._disabled ? null : _handleFilePicker,
          child: isEmpty
            ? widget.placeholder
            : widget.fileSelectedBuilder!(_file!, deleteFile: _deleteCurrentFile),
        ),
      )
    );
  }
}
