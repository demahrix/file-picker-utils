import 'dart:io' show File, Platform;

class FilePickerUtils {

  static String getFileSize(File file) {
    final int bytes = file.lengthSync();
    final int ko = bytes ~/ 1024;

    if (ko < 1000)
      return ko.toString() + " ko";

    return (ko / 1024).toStringAsFixed(2) + " Mo";
  }

  static String getFileName(File file) {
    final String path = file.path;
    final int index = path.lastIndexOf(Platform.pathSeparator);
    if (index == -1)
      return path;
    return path.substring(index + 1);
  }

  static String getLastModified(File file) {
    final DateTime date = file.lastModifiedSync();
    return "${_addO(date.day)}/${_addO(date.month)}/${_addO(date.year)}";
  }

  static String _addO(int n) {
    return n < 10 ? "0" + n.toString() : n.toString();
  }

  static String getImageUrl(String path) {
    final int index = path.lastIndexOf(".");
    final String? extension = index == - 1 ? null : path.substring(index + 1);

    switch(extension) {
      case "pdf":
        return "assets/pdf.png";
      case "doc":
      case "docx":
        return "assets/word.png";
      case "ppt":
      case "pptx":
        return "assets/powerpoint.png";
      case "xlsx":
        return "assets/excel.png";
      case "jpeg":
      case "jpg":
      case "png":
      case "webp":
      case "svg":
        return "assets/image.png";
      case "mp4":
        return "assets/video.png";
      case "mp3":
        return "assets/music.png";
      case "zip":
      case "rar":
        return "assets/zip.png";
      default:
        return "assets/other.png";
    }

  }

}