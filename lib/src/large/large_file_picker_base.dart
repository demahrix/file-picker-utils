
import 'dart:io';
import 'package:file_picker_utils/file_picker_utils.dart';
import 'package:flutter/material.dart';
import 'package:file_picker_utils/src/file_picker_controller.dart';
import 'package:filepicker_windows/filepicker_windows.dart';

// Ne pas oublier le button supprimer
class LargeFilePicker extends StatefulWidget {

  final String label;
  final Map<String, String> filterSpecification;
  final ValueChanged<File?> onChanged;
  final FilePickerController? controller;
  final bool showDeleteButton;

  LargeFilePicker({
    required this.label,
    this.filterSpecification: const {},
    required this.onChanged,
    this.controller,
    required this.showDeleteButton
  });

  @override
  _LargeFilePickerState createState() => _LargeFilePickerState();
}

class _LargeFilePickerState extends State<LargeFilePicker> {

  File? _file;

  @override
  void initState() {
    super.initState();

    if (widget.controller != null) {
      widget.controller!.open = _onPickFile;
      widget.controller!.reset = _deleteFile;
    }

  }

  void _onPickFile() {

    final result = OpenFilePicker()
      ..filterSpecification = widget.filterSpecification
      ..title = widget.label;

    final file = result.getFile();

    if (file != null) {
      this.setState(() { _file = file; });
      widget.onChanged(file);
    }

  }

  void _deleteFile() {
    this.setState(() { _file = null; });
    widget.onChanged(null);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onPickFile,
      child: Container(
        height: 70.0,
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          color: Colors.blue.withOpacity(0.1),
          border: Border.all(color: Colors.blue, width: 1.0),
          borderRadius: const BorderRadius.all(Radius.circular(12.0))
        ),
        child: _file == null ? _placeholderWidget() : _fileSelectWidget(_file!),
      ),
    );
  }

  static Widget _placeholderWidget() {
    return Row(
      children: [

        // changer pour correspondre avec le type de l'image
        const Icon(Icons.attach_file, color: Colors.blue),

        const SizedBox(width: 15.0),

        Text(
          "Veuillez selectionner un fichier",
          style: TextStyle(
            color: Colors.blue
          ),
        )
      ],
    );
  }

  Widget _fileSelectWidget(File file) {
    return Row(
      children: [

        SizedBox(
          width: 60.0,
          child: Center(child: Image.asset(FilePickerUtils.getImageUrl(file.path), package: "file_picker_utils", width: 40.0,)),
        ),

        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [

              Text(
                FilePickerUtils.getFileName(file),
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: Colors.blue
                ),
              ),

              const SizedBox(height: 5.0),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Text(
                    FilePickerUtils.getFileSize(file),
                    style: TextStyle(
                      color: Colors.blue
                    ),
                  ),

                  Text(
                    FilePickerUtils.getLastModified(file),
                    style: TextStyle(
                      color: Colors.blue
                    ),
                  )

                ],
              )
            ],

          ),
        ),

        GestureDetector(
          onTap: _deleteFile,
          child: const SizedBox(
            width: 40.0,
            child: Icon(Icons.close),
          ),
        )

      ],
    );
  }

}
